Rails.application.routes.draw do
  get '/', :to => 'default#index', as: 'index'
  get '/goodbye', :to => 'default#goodbye', as: 'goodbye'

  root 'default#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
