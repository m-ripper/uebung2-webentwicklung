require 'test_helper'

class DefaultControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get default_index_url
    assert_response :success
  end

  test "should get goodbye" do
    get default_goodbye_url
    assert_response :success
  end

end
